# macrominds/provision/uberspace/lib-webp

Installs lib webp on an U7 uberspace.

Use webp to convert images to the (often) very compact webp format:

```bash
$ cwebp my.jpg -progress -o my.webp
```

See [macrominds/convert-images](https://gitlab.com/macrominds/convert-images) if you want to 
batch scale images as crisp as possible and convert them to jpg and webp.  

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

You can parameterize this role on different levels. 
See [defaults/main.yml](defaults/main.yml). 

* `lib_webp_installation_target_folder`: The absolute path to the installation target folder. 
  Defaults to `{{ lib_webp_user_home_folder }}/local`.
* `lib_webp_user_home_folder`: the absolute path to the user's home folder. 
  Defaults to `/home/{{ lib_webp_user_name }}`.
* `lib_webp_user_name`: the user name of your Uberspace account. 
  Defaults to the user as gathered by ansible.


* `lib_webp_download_url`: Defaults to `{{ lib_webp_download_url_prefix }}{{ lib_webp_variant }}{{ lib_webp_download_url_suffix }}`
* `lib_webp_download_url_prefix`: Defaults to `https://storage.googleapis.com/downloads.webmproject.org/releases/webp/`
* `lib_webp_variant`: Used to determine download location and installation target, as well as `$PATH`. 
  Defaults to `libwebp-0.4.1-linux-x86-64`.
* `lib_webp_download_url_suffix`: Defaults to `.tar.gz`

* `lib_webp_extra_path_line`: the line to be inserted into `~/.bash_profile`.
  Defaults to `PATH={{ lib_webp_path_env_fragment }}:$PATH }}`
* `lib_webp_path_env_fragment`: The path added to `$PATH`. 
  Defaults to `{{ lib_webp_installation_target_folder }}/{{ lib_webp_variant }}/bin`

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/lib-webp.git
  path: roles
  name: lib-webp
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Fact based username

```yaml
---
- hosts: all
  roles:
    - role: lib-webp
```

### Specified username

```yaml
---
- hosts: all
  roles:
    - { role: lib-webp, lib_webp_user_name: secret }
```

## Example ad-hoc

Run this from ``$PROJECT_ROOT/../`` to provision any U7 space:

```bash
ansible-playbook -i my-server-with-trailing-comma, lib-webp/molecule/default/playbook.yml 
```

Replace `my-server-with-trailing-comma` with a HostName that is configured in your ~/.ssh/config. 
Make sure to add a trailing comma, otherwise, ansible expects an inventory file instead of a HostName.

## License

ISC

## Author Information

This role was created in 2019 by Thomas Praxl.
